Config                        = {}
Config.Locale                 = 'en'

Config.VaultBox = 'p_v_43_safe_s'
Config.Vault = {
	vault = {
		coords = vector3(467.94, -1011.17, 34.93),
		heading = 176.83,
		needItemLicense = '', --'licence_vault' -- If you don't want to use items Allow you to leave it blank or needItemLicense = nil
		InfiniteLicense = true -- Should one License last forever?
	},
	police = {
		coords = vector3(482.19, -982.74, 23.91),
		heading = 264.06,
	},
	ambulance = {
		coords = vector3(353.5, -578.67, 27.79),
		heading = 157.92,
	},
	mechanic = {
		coords = vector3(-328.59, -140.51, 38.01),
		heading = 251.05,
	},
	biker = {
		coords = vector3(955.18, -119.33, 74.75),
		heading = 117.00,
	}
}