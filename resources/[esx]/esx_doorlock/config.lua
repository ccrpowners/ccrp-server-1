Config = {}
Config.Locale = 'en'

-------------------
--- toggletype = 0 // Just need a code
--- toggletype = 1 // You need a job
--- toggletype = 2 // You need the code and the job
--------------

Config.DoorList = {

	--
	-- Mission Row First Floor
	--

	-- To locker room & roof
	{
		objName = 'v_ilev_ph_gendoor004',
		objCoords  = {x = 449.698, y = -986.469, z = 30.689},
		textCoords = {x = 450.104, y = -986.388, z = 31.739},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	-- Rooftop
	{
		objName = 'v_ilev_gtdoor02',
		objCoords  = {x = 464.361, y = -984.678, z = 43.834},
		textCoords = {x = 464.361, y = -984.050, z = 44.834},
		authorizedCodes = { '1775'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true
	},

	-- Hallway to roof
	{
		objName = 'v_ilev_arm_secdoor',
		objCoords  = {x = 461.286, y = -985.320, z = 30.839},
		textCoords = {x = 461.50, y = -986.00, z = 31.50},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	-- Armory
	--[[{
		objName = 'v_ilev_arm_secdoor',
		objCoords  = {x = 452.618, y = -982.702, z = 30.689},
		textCoords = {x = 453.079, y = -982.600, z = 31.739},
		authorizedCodes = { '1337'},
		locked = true
	},]]

	-- Captain Office
	{
		objName = 'v_ilev_ph_gendoor002',
		objCoords  = {x = 447.238, y = -980.630, z = 30.689},
		textCoords = {x = 447.200, y = -980.010, z = 31.739},
		authorizedCodes = { '3791'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true
	},

	-- To downstairs (double doors)
	{
		objName = 'v_ilev_ph_gendoor005',
		objCoords  = {x = 443.97, y = -989.033, z = 30.6896},
		textCoords = {x = 444.020, y = -989.445, z = 31.739},
		authorizedCodes = { '1337'},
		authorizedJob = { 'ambulance','police'},
		toggletype = 1,
		locked = true,
		distance = 4
	},
	
	{
		objName = 'v_ilev_ph_gendoor005',
		objCoords  = {x = 445.37, y = -988.705, z = 30.6896},
		textCoords = {x = 445.350, y = -989.445, z = 31.739},
		authorizedCodes = { '1337'},
		authorizedJob = { 'admin'},
		toggletype = 1,
		locked = true,
		distance = 4
	},

	--Parking Lot Double Doors
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 444.621, y = -999.001, z = 30.788},
		textCoords = {x = 444.621, y = -999.001, z = 30.788},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 4
	},
	
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 447.218, y = -999.002, z = 30.789},
		textCoords = {x = 447.218, y = -999.002, z = 30.789},
		authorizedCodes = { '1337'},
		authorizedJob = { 'admin'},
		toggletype = 1,
		locked = true,
		distance = 4
	},
	
	-- 
	-- Mission Row Upstairs
	--

	--Dispatch Door 1
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = {x = 444.74730, y = -988.83990, z = 36.05274},
		textCoords = {x = 444.74730, y = -988.83990, z = 36.05274},
		authorizedCodes = { '5'},
		authorizedJob = { 'admin'},
		toggletype = 0,
		locked = true,
		distance = 4
	},

	--Dispatch Door 2
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = {x = 449.84080, y = -982.46700, z = 36.04901},
		textCoords = {x = 449.84080, y = -982.46700, z = 36.04901},
		authorizedCodes = { '5'},
		authorizedJob = { 'admin'},
		toggletype = 0,
		locked = true,
		distance = 4
	},

	--Main Door 2nd Floor
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = {x = 449.80230, y = -992.06960, z = 36.04901},
		textCoords = {x = 449.80230, y = -992.06960, z = 36.04901},
		authorizedCodes = { '5'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 4
	},

	--Executive Office
	{
		objName = 'v_ilev_ph_gendoor002',
		objCoords  = {x = 463.41680, y = -1001.01500, z = 36.05486},
		textCoords = {x = 463.41680, y = -1001.01500, z = 36.05486},
		authorizedCodes = { '8047'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 4
	},

	-- 
	-- Mission Row Downstairs
	--
	--Medical Bay
	{
		objName = 'v_ilev_ph_gendoor003',
		objCoords  = {x = 438.47100, y = -979.55300, z = 26.82234},
		textCoords = {x = 438.47100, y = -979.55300, z = 26.82234},
		authorizedCodes = { '5150'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 2
	},

	--Evidence/Storage
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = {x = 471.31540, y = -986.10910, z = 25.05795},
		textCoords = {x = 471.31540, y = -986.10910, z = 25.05795},
		authorizedCodes = { '9876'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 4
	},

	--Server Room
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = {x = 467.59360, y = -977.99330, z = 25.05795},
		textCoords = {x = 467.59360, y = -977.99330, z = 25.05795},
		authorizedCodes = { '8675309'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 4
	},

	--Lab
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = {x = 463.61460, y = -980.58140, z = 25.05795},
		textCoords = {x = 463.61460, y = -980.58140, z = 25.05795},
		authorizedCodes = { '1337'},
		authorizedJob = { 'ambulance','police'},
		toggletype = 1,
		locked = true,
		distance = 4
	},

	--Dispatch Director Office
	{
		objName = 'v_ilev_ph_gendoor003',
		objCoords  = {x = 444.19480, y = -979.55280, z = 26.81845},
		textCoords = {x = 444.19480, y = -979.55280, z = 26.81845},
		authorizedCodes = { '69420'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 4
	},

	-- 
	-- Mission Row Cells
	--

	-- Main Cells
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 463.815, y = -992.686, z = 24.9149},
		textCoords = {x = 463.815, y = -992.686, z = 24.9149},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2
	},

	-- Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.381, y = -993.651, z = 24.914},
		textCoords = {x = 461.806, y = -993.308, z = 25.064},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	-- Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.331, y = -998.152, z = 24.914},
		textCoords = {x = 461.806, y = -998.800, z = 25.064},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	-- Cell 3
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.704, y = -1001.92, z = 24.9149},
		textCoords = {x = 461.806, y = -1002.450, z = 25.064},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	--Drunk Tank 1
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 467.19220, y = -996.45940, z = 25.00599},
		textCoords = {x = 467.19220, y = -996.45940, z = 25.00599},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	--Drunk Tank 2
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 471.47550, y = -996.45940, z = 25.00599},
		textCoords = {x = 471.47550, y = -996.45940, z = 25.00599},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	--Drunk Tank 3
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 475.75430, y = -996.45940, z = 25.00599},
		textCoords = {x = 475.75430, y = -996.45940, z = 25.00599},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	--Drunk Tank 4
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 480.03010, y = -996.45940, z = 25.00599},
		textCoords = {x = 480.03010, y = -996.45940, z = 25.00599},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	--Interview 1 (interagation)
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 468.48720, y = -1003.54800, z = 25.01314},
		textCoords = {x = 468.48720, y = -1003.54800, z = 25.01314},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	--Interview 2 (interagation)
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 477.04970, y = -1003.55300, z = 25.01203},
		textCoords = {x = 477.04970, y = -1003.55300, z = 25.01203},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	-- To Sallyport
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 463.478, y = -1003.538, z = 25.005},
		textCoords = {x = 464.00, y = -1003.50, z = 25.50},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true
	},

	-- Back (double doors)
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 467.371, y = -1014.452, z = 26.536},
		textCoords = {x = 468.09, y = -1014.452, z = 27.1362},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 4
	},

	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 469.967, y = -1014.452, z = 26.536},
		textCoords = {x = 469.35, y = -1014.452, z = 27.136},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 4
	},

	--
	-- Mission Row PD Gates
	--

	-- Front Gate
	{
		objName = 'Prop_Gate_airport_01',
		objCoords  = {x = 411.94, y = -1025.39, z = 29.33},
		textCoords = {x = 411.94, y = -1025.39, z = 29.33},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 14,
		size = 2
	},

	-- Parking Lot Side Gate
	{
		objName = 'Prop_BS_Map_Door_01',
		objCoords  = {x = 423.85, y = -998.05, z = 30.77},
		textCoords = {x = 423.85, y = -998.05, z = 30.77},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Back Gate
	{
		objName = 'hei_prop_station_gate',
		objCoords  = {x = 488.894, y = -1017.210, z = 27.146},
		textCoords = {x = 488.894, y = -1020.210, z = 30.00},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 14,
		size = 2
	},
	
	--
	-- Hospital
	--

	--Office Double Doors
	{
		objName = 'v_ilev_gc_door02',
		objCoords  = {x = 303.302, y = -1460.063, z = 38.061},
		textCoords = {x = 303.302, y = -1460.063, z = 38.061},
		authorizedCodes = { '303'},
		authorizedJob = { 'ambulance','police'},
		toggletype = 0,
		locked = true,
		distance = 2.5,
	},

	{
		objName = 'v_ilev_gc_door02',
		objCoords  = {x = 304.973, y = -1458.071, z = 38.061},
		textCoords = {x = 304.973, y = -1458.071, z = 38.061},
		authorizedCodes = { '303'},
		authorizedJob = { 'ambulance','police'},
		toggletype = 0,
		locked = true,
		distance = 2.5,
	},

	--Hospital Pharacy Entrance
	{
		objName = 'v_ilev_fib_door1',
		objCoords  = {x = 293.08018, y = -1458.12400, z = 30.12085},
		textCoords = {x = 293.08018, y = -1458.12400, z = 30.12085},
		authorizedCodes = { '303'},
		authorizedJob = { 'ambulance','police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Hospital Pharacy Exit
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 304.53490, y = -1468.25700, z = 30.11466},
		textCoords = {x = 304.53490, y = -1468.25700, z = 30.11466},
		authorizedCodes = { '303'},
		authorizedJob = { 'ambulance'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Hospital Duty Room
	{
		objName = 'v_ilev_fib_door1',
		objCoords  = {x = 287.05440, y = -1451.52800, z = 30.12085},
		textCoords = {x = 287.05440, y = -1451.52800, z = 30.12085},
		authorizedCodes = { '303'},
		authorizedJob = { 'ambulance'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--
	-- Sandy Shores
	--

	--Entrance
	{
		objName = 'v_ilev_shrfdoor',
		objCoords  = {x = 1855.105, y = 3683.516, z = 34.266},
		textCoords = {x = 1855.105, y = 3683.516, z = 34.266},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Sheriff Office
	{
		objName = 'v_ilev_shrfdoor',
		objCoords  = {x = 1859.518, y = 3687.894, z = 34.568},
		textCoords = {x = 1859.518, y = 3687.894, z = 34.568},
		authorizedCodes = { '3004'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 2.5,
	},

	-- Office Door
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 1857.466, y = 3689.996, z = 34.400},
		textCoords = {x = 1857.466, y = 3689.996, z = 34.400},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Sandy Office Back Door
	{
		objName = 'v_ilev_gc_door01',
		objCoords  = {x = 1854.107, y = 3700.163, z = 34.402},
		textCoords = {x = 1854.107, y = 3700.163, z = 34.402},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Sandy Armory
	{
		objName = 'v_ilev_cd_entrydoor',
		objCoords  = {x = 1844.318, y = 3694.140, z = 34.404},
		textCoords = {x = 1844.318, y = 3694.140, z = 34.404},
		authorizedCodes = { '1337'},
		authorizedJob = { 'admin'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},
	
	--Sandy Hallway
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 1848.856, y = 3690.847, z = 34.402},
		textCoords = {x = 1848.856, y = 3690.847, z = 34.402},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Sandy Shores Booking Office
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 1850.446, y = 3683.377, z = 34.402},
		textCoords = {x = 1850.446, y = 3683.377, z = 34.402},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Sandy Shores Jail Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 1846.639, y = 3685.481, z = 34.40025},
		textCoords = {x = 1846.639, y = 3685.481, z = 34.40025},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--Sandy Shores Jail Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 1845.17, y = 3688.78, z = 34.26},
		textCoords = {x = 1845.17, y = 3688.78, z = 34.26},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--
	-- Bolingbroke Penitentiary
	--

	-- Entrance (Two big gates)
	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1844.998, y = 2604.810, z = 44.638},
		textCoords = {x = 1844.998, y = 2608.50, z = 48.00},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 12,
		size = 2
	},

	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1818.542, y = 2604.812, z = 44.611},
		textCoords = {x = 1818.542, y = 2608.40, z = 48.00},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 12,
		size = 2
	},

	--Side Gate
	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1799.610, y = 2616.975, z = 44.599},
		textCoords = {x = 1799.610, y = 2616.975, z = 44.599},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 12,
		size = 2
	},

		--Pillbox Hill Pharmacy Door 1
		{
			objName = 'v_ilev_cor_firedoor',
			objCoords  = {x = 346.031, y = -574.188, z = 28.797},
			textCoords = {x = 346.031, y = -574.188, z = 28.797},
			authorizedCodes = { '1337'},
			authorizedJob = { 'ambulance'},
			toggletype = 1,
			locked = true,
			distance = 2.5,
		},

		--Pillbox Hill Pharmacy Door 2
		{
			objName = 'v_ilev_cor_firedoor',
			objCoords  = {x = 346.818, y = -572.025, z = 28.797},
			textCoords = {x = 346.818, y = -572.025, z = 28.797},
			authorizedCodes = { '1337'},
			authorizedJob = { 'ambulance'},
			toggletype = 1,
			locked = true,
			distance = 2.5,
		},

	--OR Suite Door 1
	{
		objName = 'v_ilev_cor_firedoor',
		objCoords  = {x = 342.178, y = -570.347, z = 28.818},
		textCoords = {x = 342.178, y = -570.347, z = 28.818},
		authorizedCodes = { '1337'},
		authorizedJob = { 'ambulance'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--OR Suite Door 2
	{
		objName = 'v_ilev_cor_firedoor',
		objCoords  = {x = 341.392, y = -572.507, z = 28.818},
		textCoords = {x = 341.392, y = -572.507, z = 28.818},
		authorizedCodes = { '1337'},
		authorizedJob = { 'ambulance'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--
	-- Paleto Sheriff Office
	--

	-- Main Gate
	{
		objName = 'p_gate_prison_01_s',
		objCoords  = {x = -450.45170, y = 6025.05000, z = 30.49273},
		textCoords = {x = -450.45170, y = 6025.05000, z = 30.49273},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 12,
		size = 2
	},

	-- Main Office Door 1
	{
		objName = 'v_ilev_bk_door2',
		objCoords  = {x = -440.980, y = 6012.771, z = 31.866},
		textCoords = {x = -440.980, y = 6012.771, z = 31.866},
		authorizedCodes = { '1337'},
		authorizedJob = { 'admin'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Main Office Door 2
	{
		objName = 'v_ilev_bk_door2',
		objCoords  = {x = -442.821, y = 6010.931, z = 31.866},
		textCoords = {x = -442.821, y = 6010.931, z = 31.866},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Main Office Door 3
	{
		objName = 'v_ilev_ss_door8',
		objCoords  = {x = -447.709, y = 6006.717, z = 31.808},
		textCoords = {x = -447.709, y = 6006.717, z = 31.808},
		authorizedCodes = { '1337'},
		authorizedJob = { 'admin'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Main Office Door 4
	{
		objName = 'v_ilev_ss_door7',
		objCoords  = {x = -449.550, y = 6008.558, z = 31.808},
		textCoords = {x = -449.550, y = 6008.558, z = 31.808},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Interrogation Room
	{
		objName = 'v_ilev_cd_entrydoor',
		objCoords  = {x = -454.536, y = 6011.258, z = 31.869},
		textCoords = {x = -454.536, y = 6011.258, z = 31.869},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Armory
	{
		objName = 'v_ilev_fingate',
		objCoords  = {x = -437.614, y = 5992.819, z = 31.936},
		textCoords = {x = -437.614, y = 5992.819, z = 31.936},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Jail Cell 1
	{
		objName = 'v_ilev_fingate',
		objCoords  = {x = -437.614, y = 5992.819, z = 31.936},
		textCoords = {x = -437.614, y = 5992.819, z = 31.936},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Jail Cell 2
	{
		objName = 'v_ilev_fingate',
		objCoords  = {x = -437.614, y = 5992.819, z = 31.936},
		textCoords = {x = -437.614, y = 5992.819, z = 31.936},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	-- Sheriff Office
	{
		objName = 'v_ilev_cf_officedoor',
		objCoords  = {x = -441.053, y = 6004.997, z = 31.864},
		textCoords = {x = -441.053, y = 6004.997, z = 31.864},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 2.5,
	},

	--
	-- Addons
	--

	--[[
	-- Zancudo Military Base Front Entrance
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -1587.23, y = 2805.08, z = 15.82},
		textCoords = {x = -1587.23, y = 2805.08, z = 19.82},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 12,
		size = 2
	},
	
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -1600.29, y = 2793.74, z = 15.74},
		textCoords = {x = -1600.29, y = 2793.74, z = 19.74},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 0,
		locked = true,
		distance = 12,
		size = 2
	},
	
	
	-- Zancudo Military Base Back Entrance
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -2296.17, y = 3393.1, z = 30.07},
		textCoords = {x = -2296.17, y = 3393.1, z = 34.07},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 12,
		size = 2
	},
	
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -2306.13, y = 3379.3, z = 30.2},
		textCoords = {x = -2306.13, y = 3379.3, z = 34.2},
		authorizedCodes = { '1337'},
		authorizedJob = { 'police'},
		toggletype = 1,
		locked = true,
		distance = 12,
		size = 2
	},]]--
}