ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('duty:onoff')
AddEventHandler('duty:onoff', function(job)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local job = xPlayer.job.name
    local grade = xPlayer.job.grade
    
    if job == 'police' or job == 'ambulance' or job == 'taxi' then
        xPlayer.setJob('off' ..job, grade)
        TriggerClientEvent('esx:showNotification', _source, _U('offduty'))
        exports.ft_libs:EnableArea("esx_eden_garage_area_police_mecanodeletepoint")
        exports.ft_libs:EnableArea("esx_eden_garage_area_police_mecanospawnpoint")
        exports.ft_libs:EnableArea("esx_eden_garage_area_Bennys_mecanodeletepoint")
        exports.ft_libs:EnableArea("esx_eden_garage_area_Bennys_mecanospawnpoint")
    elseif job == 'offpolice' then
        xPlayer.setJob('police', grade)
        TriggerClientEvent('esx:showNotification', _source, _U('onduty'))
        exports.ft_libs:DisableArea("esx_eden_garage_area_police_mecanodeletepoint")
        exports.ft_libs:DisableArea("esx_eden_garage_area_police_mecanospawnpoint")
        exports.ft_libs:DisableArea("esx_eden_garage_area_Bennys_mecanodeletepoint")
        exports.ft_libs:DisableArea("esx_eden_garage_area_Bennys_mecanospawnpoint")
    elseif job == 'offambulance' then
        xPlayer.setJob('ambulance', grade)
        TriggerClientEvent('esx:showNotification', _source, _U('onduty'))
	elseif job == 'offtaxi' then
        xPlayer.setJob('taxi', grade)
        TriggerClientEvent('esx:showNotification', _source, _U('onduty'))
    end

end)