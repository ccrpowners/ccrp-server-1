Locales['en'] = {
	['ooc_help'] = 'type an out of character message',
	['ooc_prefix'] = 'OOC | %s',
	['do_help'] = 'rP information',
	['do_prefix'] = 'do | %s',
	['news_help'] = 'announce news (do not abuse)',
	['news_prefix'] = 'news | %s',
	['ooc_argument_name'] = 'message',
	['ooc_argument_help'] = 'the message',
	['ooc_unknown_command'] = '^3%s^0 is not a valid command!',
}
