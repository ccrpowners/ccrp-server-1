----------------------------------------
--- Discord Whitelist, Made by FAXES ---
----------------------------------------

--- Config ---
--roleNeeded = "Civilian" -- The role nickname needed to pass the whitelist
--roleNeeded = "Dev"
roleNeeded = "TESTER"
--notWhitelisted = "This server is currently locked down to Owners Only." --Dev Message
notWhitelisted = "This server is currently in Development Mode." --Tester Message
noDiscord = "You must have Discord open to join this server." -- Message displayed when discord is not found


--- Code ---

AddEventHandler("playerConnecting", function(name, setCallback, deferrals)
    local src = source
    deferrals.defer()
    deferrals.update("Checking Permissions")

    for k, v in ipairs(GetPlayerIdentifiers(src)) do
        if string.sub(v, 1, string.len("discord:")) == "discord:" then
            identifierDiscord = v
        end
    end

    if identifierDiscord then
        if exports.discord_perms:IsRolePresent(src, roleNeeded) then
            deferrals.done()
        else
            deferrals.done(notWhitelisted)
        end
    else
        deferrals.done(noDiscord)
    end
end)