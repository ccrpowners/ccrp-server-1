# you probably don't want to change these!
# only change them if you're using a server with multiple network interfaces
endpoint_add_tcp "0.0.0.0:30120"
endpoint_add_udp "0.0.0.0:30120"

set es_enableCustomData 1
set mysql_connection_string "server=localhost;database=essentialmode;userid=root;"
set sv_licenseKey "qqk4k5c6umckz7azgd7r2o1dq409jswx"

add_ace resource.essentialmode command.sets allow
add_ace resource.essentialmode command.add_principal allow
add_ace resource.essentialmode command.add_ace allow


sets tags "seriousrp, fun, policerp, customcars, fire, cad/mdt, doj, roleplay, economy, whitelistedpolice, whitelistedfire" 
sets Discord "https://discord.gg/GXjttC7"
sets Website "https://forums.commercialcityrp.com"
#sets Teamspeak ""

exec permissions.cfg



### ESSENTIAL ####
start scoreboard
start chat
#start es_admin2
start es_camera
start esplugin_mysql
start essentialmode
start es_extended
start fivem
start hardcap
start mapmanager
start mysql-async
start rconlog
start sessionmanager
start spawnmanager

#### Basics ####
start live_map
start baseevents
start bob74_ipl
start commands
start cron
start deleteveh
#start mellotrainer
start fivem-map-skater
start instance
start NoDriveBy
start JF_SeatShuffle
start PvP
start custom-loadout
start skinchanger
start streetLabel
start VK_main
start wk_vehdamage
start deathmessages

#### ESX ####
start new_banking
start esx_addonaccount
start esx_phone
start esx_addoninventory
start esx_aiomenu
start esx_ambulancejob
start esx_animations
start esx_barbershop
start esx_basicneeds
start esx_billing
start esx_carwash
start esx_clotheshop
start esx_datastore
start esx_deliveryjob
start esx_dmvschool
start esx_drugs
start esx_jb_eden_garage-master
start esx_firejob
start esx_holdup
start esx_identity
start esx_joblisting
start esx_jobs
start esx_license
start esx_lscustom
start esx_mask
start esx_mecanojob
start esx_menu_default
start esx_menu_dialog
start esx_menu_list
start esx_policejob
#start esx_property
start esx_realestateagentjob
start esx_service
#start esx_shops
start esx_inventoryhud_trunk
start esx_inventoryhud
start esx_supermarket
start esx_skin
#start esx_society --Broken and have no idea how to fix
start esx_status
start esx_taxijob
start esx_truckerjob
start esx_vehicleshop
start esx_voice
start esx_weashops
start esx_brinksjob
start esx_gopostal
start esx_holdupbank
start esx_airlines
start esx_optionalneeds
start esx_mafia
start esx_rebel
start esx_coffee
start esx_securityjob
start esx_pizza
start esx_bennys
start esx_garbagejob
start esx_tattoosShops
start esx_ranger
start esx_clip
start esx_bus
start esx_dockshop
start esx_coastguard
start esx_lsd-master
start esx_turtle
start announce-noesx
#start chatforadmin-noesx
start replyandreport-noesx
start idonlyforadmin-noesx
#start esx_rpchat
start esx_bishops
start esx_litter
start esx_dismay
start esx_grove
start ESX_binoculars
start esx_plasticsurgery
start esx_vagos
start esx_ballas
start esx_moneywash
start esx_cigarette
start esx_jailer-master
start esx_Armour
start esx_detector
start esx_damage
#start esx_criminalrecords
start esx_traffic
start esx_ktackle
start esx_poolcleaner
#start esx_panicbutton
start esx_carthief
start esx_gym
start esx_gym
start esx_diving
start Esx_Ocean
start esx_taximeter
start esx_scratchoffs
start esx_cratedelivery
start esx_vehiclelock
start esx_hunting
start esx_legacyfuel
start esx_teleports

#### Cars ####
#start AirOne
#start ambulance
#start ambulance3
#start BC
#start BCT
#start bounty2
#start Brush
#start dcd
#start EMSC
#start emsfpiu
#start express
#start F450
#start freightraised
#start IA
start k9
#start LEO
#start LEO2
#start LEO3
#start LEO4
#start LEO5
#start LEO6
#start lspdpack
#start pdbike
#start police8
#start Pump
#start Rescue
#start riot
#start sheriff2
#start supervolito
#start TM
#start Tower
#start UDC
#start umraptor

#### Maps ####
start mirrorpark3.0
start MissionRowPD
start ogpack
start SandyShoresTowYard
start ssfd
start Traffic
start Firestation
start coffe_policestation
start firestationfix
start movepole
start chumash

#### Other Addons ####
start carhud
#start eup-ui
#start vSync
#start codurly_cadConnect
start Area-of-Patrol-master
#start eup-stream
start guns
start handsup
start hospital
start Jailer
start knockout
start LEOAnimations
start skins
start map
start nearest-postal-1.3
#start fire_fivem
start InteriorLights
start RPChat-master
start lux_vehcontrol
start pointing
start EasyAdmin
start NativeUI
start wraith_alpr
start pv-tow
#start vengine
#start vehcontrol
start PTTPoliceRadio
start marker
start eden_jail
start hat
start drop
start eden_accesories
start nos
#start eden_animal-master
#start welcome
#start Material_Load-Loading-Screen
#start clothes
start FCV
start chopshop
start vannouncer
#start pdlock
#start PoliceVehiclesWeaponDeleter
start pNotify
start InteractSound
start blips
#start allcity_wallet
start MovieView
start weazel
#start shitscript
#start Hookers   needs fixed spawns too many girls
start stasiek_selldrugs-master
start cratedrop
start securitycams
start Radiant_gsrtest
start crouch
start seatbelt
#start DiscordBot
start Discord 
start unmarked-police-pack
start CalmAI
start disabledispatch
start RealAITraffic
start vehicle-control
start staff
start vMenu
start RJsBeta
start c_policestation_needs
start watermark
start FireScript
start holograms
start BlipsBuilder
start esx_doorlock
start esx-kr-vehicle-push
start c_policestation_needs
start discord_perms
start DiscordWhitelist


sv_scriptHookAllowed 0

# change this
rcon_password 1471070427

sv_hostname "^1Commercial City RolePlay ^0| ^4Economy | ^3Serious RP ^0| ^4Custom Jobs ^0| ^3CCRP ^0| ^4RocketCAD ^0"

# nested configs!
#exec server_internal.cfg

# loading a server icon (96x96 PNG file)
load_server_icon servericon.png

# convars for use from script
set temp_convar "hey world!"

# disable announcing? clear out the master by uncommenting this
#sv_master1 ""

# want to only allow players authenticated with a third-party provider like Steam?
#sv_authMaxVariance 1
#sv_authMinTrust 5

##AOP###

add_ace Fax.AOP faxes.aopcmds allow

add_principal identifier.steam:110000132580eb0 Fax.AOP # K9
add_principal identifier.steam:11000010a01bdb9 Fax.AOP # Sticky

# add system admins
add_ace group.admin command allow # allow all commands

add_ace group.admin command.quit deny # but don't allow quit
add_principal identifier.steam:11000010a01bdb9 group.admin command allow

##EasyAdmin###
add_ace group.owner easyadmin allow
add_ace group.owner easyadmin.manageserver allow
add_ace group.owner easyadmin.addadmin allow
add_ace group.admin easyadmin.manageserver allow
add_ace group.admin easyadmin.addadmin allow
add_ace group.looser easyadmin.kick allow
add_ace group.looser easyadmin.freeze allow #regmod/retired
add_ace group.superlooser easyadmin.kick allow #retiredmed
add_ace group.superlooser easyadmin.freeze allow
add_ace group.superlooser easyadmin.spectate allow
add_ace group.awesomelooser easyadmin.kick allow #retiredhigh
add_ace group.awesomelooser easyadmin.freeze allow
add_ace group.awesomelooser easyadmin.spectate allow
add_ace group.awesomelooser easyadmin.slap allow
add_ace group.watchdog easyadmin.spectate allow
add_ace group.helper easyadmin.spectate allow
add_ace group.helper easyadmin.teleport allow
add_ace group.moderator easyadmin.ban allow
add_ace group.moderator easyadmin.kick allow
add_ace group.moderator easyadmin.spectate allow
add_ace group.moderator easyadmin.teleport allow
add_ace group.moderator easyadmin.slap allow
add_ace group.moderator easyadmin.freeze allow
add_ace group.trialadmin easyadmin.ban allow
add_ace group.trialadmin easyadmin.kick allow
add_ace group.trialadmin easyadmin.spectate allow
add_ace group.trialadmin easyadmin.teleport allow
add_ace group.trialadmin easyadmin.freeze allow
add_ace group.senioradmin easyadmin.ban allow
add_ace group.senioradmin easyadmin.kick allow
add_ace group.senioradmin easyadmin.spectate allow
add_ace group.senioradmin easyadmin.unban allow
add_ace group.senioradmin easyadmin.teleport allow
add_ace group.senioradmin easyadmin.slap allow
add_ace group.senioradmin easyadmin.freeze allow
add_ace group.trusted easyadmin.ban allow
add_ace group.trusted easyadmin.kick allow
add_ace group.trusted easyadmin.spectate allow
add_ace group.trusted easyadmin.unban allow
add_ace group.trusted easyadmin.teleport allow
add_ace group.trusted easyadmin.slap allow
add_ace group.trusted easyadmin.freeze allow
add_ace group.trusted easyadmin.manageserver allow
add_ace group.pillsbury easyadmin.ban allow
add_ace group.pillsbury easyadmin.kick allow
add_ace group.pillsbury easyadmin.spectate allow
add_ace group.pillsbury easyadmin.unban allow
add_ace group.pillsbury easyadmin.teleport allow
add_ace group.pillsbury easyadmin.slap allow
add_ace group.pillsbury easyadmin.freeze allow
add_ace group.pillsbury easyadmin allow
add_ace group.pillsbury easyadmin.manageserver allow
add_ace group.pillsbury easyadmin.addadmin allow
add_ace group.pillsbury easyadmin.immune allow
add_ace group.pillsbury easyadmin.screenshot allow

add_principal identifier.steam:11000010a01bdb9 group.owner # Sticky
add_principal identifier.steam:110000132580eb0 group.owner # K9

# hide player endpoints in external log output
sv_endpointprivacy true

# server slots limit (default to 30)
sv_maxclients 32


restart sessionmanager